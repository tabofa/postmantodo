#!/bin/bash

echo Stopping frontend...
docker-compose -f Frontend/docker-compose.yml down

echo Stopping Backend
docker-compose -f Backend/docker-compose.yml down