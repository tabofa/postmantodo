import React, {Component} from 'react'
import Todos from './components/Todos'
import Header from './components/Header'

class App extends Component {
  render() {
    return (
      <div className="container">
        <Header/>
        <Todos/>
      </div>
    )
  }
}

export default App
