import React, {Component} from 'react';
import {Glyphicon, Button, FormGroup, ControlLabel, FormControl, HelpBlock} from 'react-bootstrap'
import {Modal} from 'react-materialize'

class Todos extends Component {
  baseURL() {
    return 'http://localhost:8181'
  }

  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          _id: -1,
          title: "Loading...",
          info: "Loading...",
          status: "1"
        },
      ],
      value: ''
    }
  }

  render() {
    return (
      <div>
        <FormGroup
          controlId="formBasicText"
        >
          <ControlLabel>Task</ControlLabel>
          <FormControl
            type="text"
            value={this.state.value}
            placeholder="Enter text"
            onChange={this.handleChange}
            onKeyDown={this.handleKeyPress}
          />
          <FormControl.Feedback/>
          <HelpBlock>Press [Enter] to add ToDo</HelpBlock>
        </FormGroup>
        <div>
          <table className="table table-striped">
            <thead>
            <tr>
              <th>ToDo</th>
              <th><span className="pull-right">Action</span></th>
            </tr>
            </thead>
            <tbody>
            {this.state.data.map((data, i) => {
              return this.renderRows(data, i)
            })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  renderRows(data, i) {
    let todo = data.title
    let checked = false
    if (data.state === "0") {
      todo = <s>{data.title}</s>
      checked = true
    }
    return (
      <tr key={data._id}>
        <td id={'todo-' + data._id} onClick={() => {
          this.toggleState(data._id)
        }}>
          <input type="checkbox" checked={checked}/> <span> {todo}</span>
        </td>
        <td>
          <span className="pull-right">
            {this.renderModal(data, i)}
            <span> </span>
          <Button bsStyle="danger" bsSize="xsmall" id={'delete-' + data._id} onClick={() => {
            this.deleteEntry(data._id)
          }}><Glyphicon glyph="trash"/> Delete</Button>
          </span>
        </td>
      </tr>
    )
  }

  renderModal(data, i) {
     return (
      <Modal
        header={'Edit todo'}
        trigger={<Button>Edit</Button>}
      >
        <p>Här kan du uppdatera din Todo, använd dig av [Enter] för att spara uppdateringen.</p>
        <p>Använd [Close] för att stänga dialogen.</p>
        <input
          onChange={this.handleModalChange}
          onKeyPress={this.handleModalKeyPress}
          value={this.state.data[i].title}
          name={this.state.data[i]._id}
        />
      </Modal>
    )
  }

  handleChange = (e) => {
    this.setState({value: e.target.value})
  }

  handleModalChange = (e) => {
    let name = e.target.name
    let data = this.state.data
    for(let i in data) {
      if(data[i]._id === name) {
        data[i].title = e.target.value
        break
      }
    }
    this.setState({data})
  }

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.onSubmit()
      this.setState({
        value: ''
      })
    }
  }

  handleModalKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.updateTodo(e.target.name, e.target.value)
    }
  }

  updateTodo = (id, title) => {
    fetch(this.baseURL() + '/update/' + id, {
      method: 'PUT',
      body: JSON.stringify({
        title: title
      })
    })
      .then(res => {
        if(res.status === 204) {
          this.getEntries()
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  onSubmit = () => {
    fetch(this.baseURL() + '/add', {
      method: "POST",
      body: JSON.stringify({
        title: this.state.value,
        info: 'some info'
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        this.getEntries()
      })
      .catch(err => {
        console.log(err)
      })
  }

  deleteEntry(id) {
    fetch(this.baseURL() + '/delete/' + id, {
      method: 'DELETE'
    })
      .then(res => {
        this.getEntries()
      })
      .catch(err => {
        console.log(err)
      })
  }

  getEntries() {
    fetch(this.baseURL() + '/getTodos')
      .then(response => {
          return response.json()
        }
      ).then(data => {
      this.setState({
        data: data
      })
    })
      .catch(err => {
        console.log(err)
      })
  }

  toggleState(id) {
    fetch(this.baseURL() + '/toggle/' + id)
      .then(res => {
        this.getEntries()
      })
      .catch(err => {
        console.log(err)
      })
  }

  getTodo(id) {
    fetch(this.baseURL() + '/getTodo/' + id)
      .then(res => {
        console.log(res)
        return res.json()
      })
      .then(json => {
        console.log(json)
        return json
      })
      .catch(err => {
        console.log(err)
      })
  }

  componentWillMount() {
    this.getEntries()
  }
}

export default Todos