import React, {Component} from 'react'
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button} from 'react-bootstrap'

class Adder extends Component {
  constructor(props) {
    super(props)
    this.state = ({
      value: ''
    })
  }

  render() {
    return (
      <form>
        <FormGroup
          controlId="formBasicText"
        >
          <ControlLabel>Task</ControlLabel>
          <FormControl
            type="text"
            value={this.state.value}
            placeholder="Enter text"
            onChange={this.handleChange}
          />
          <FormControl.Feedback/>
          <Button onClick={this.onSubmit}>Save</Button>
          <HelpBlock>Press [Enter] to add ToDo</HelpBlock>
        </FormGroup>
      </form>
    )
  }

  handleChange = (e) => {
    this.setState({value: e.target.value})
  }

  onSubmit = () => {
    fetch('localhost:8181/add', {
      method: "POST",
      body: JSON.stringify({
        title: this.state.value,
        active: true
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        console.log(res.json())
      })
  }
}

export default Adder