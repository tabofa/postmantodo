# README #

Runnable with node.js environment. Requirements

* Node.js
* NPM
* Mongoose

If you prefere to run in docker, none of the above is required.

## Puropse ##

Repo is created for API-course, learning the basics.

## Setup ##

### With docker ###

Start the project by running

```bash
./MacStart.sh
```

Stop the procjet by running

```bash
./MacStop.sh
```

I get permission denied, why?
You need to set permissions for the sh file to execute the build scripts. in the terminal run the following command

```bash
chmod 700 StartMac.sh
chmod 700 StopMac.sh
```

### Not using Docker ###

Install dependancys

```bash
cd backend
npm start dev
```

```bash
cd frontend
npm start
```

### Contribution guidelines ###

No contribution desired.

You may copy, distrubute in whatever way you want. *Use on own risk*