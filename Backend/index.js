const express = require('express');
const app = express();
const bodyParser= require('body-parser')
const MongoClient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectId;
const cors = require('cors')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())

var db

MongoClient.connect('mongodb://mongodb:27017/', (err, database) => {
  if (err) return console.log(err)
  db = database
  app.listen(8181, () => {
    console.log('listening on 8181')
  })
})

app.get('/getTodos', (req, res) => {
  db.db('todos').collection('todos').find({}).toArray((err, result) => {
    if(err) return res.status(400).json({ message: 'An error occurred' })
    res.status(200).json(result)
  });
})

app.get('/getTodo/:_id', (req, res) => {
  if(req.params._id.length !== 24) res.status(400).json({ 'message': 'Invalid ID' })
  db.db('todos').collection('todos').find({_id: ObjectId(req.params._id)}).toArray((err, todo) => {
    if(err) return res.status(400).json({ message: 'An error occurred' })
    res.status(200).json(todo)
  })
})

app.get('/toggle/:_id', (req, res) => {
  if(req.params._id.length !== 24) res.status(400).json({ 'message': 'Invalid ID' })
  db.db('todos').collection('todos').find({_id: ObjectId(req.params._id)}).toArray((err, todo) => {
    if(err) return res.status(400).json({ message: 'An error occurred' })
    db.db('todos').collection('todos').update({_id: ObjectId(req.params._id)}, { $set: { active: !todo[0].active } }, (err, result) => {
      if(err) return res.status(400).json({ message: 'An error occurred' })
      res.status(204).send()
    })
  })
})

app.post('/add', (req, res) => {
  if(req.body.title === null) return res.status(400).json({message: "Title can not be null"})
  if(req.body._id === null) return res.status(400).json({message: "ID can not be null"})
  db.db('todos').collection('todos').save(req.body, (err, database) => {
    if(err) return res.status(400).json({ message: 'An error occurred' })
    res.status(200).json(req.body)
  })
})

app.put('/update/:_id', (req, res) => {
  db.db('todos').collection('todos').update({_id: ObjectId(req.params._id)}, { $set: { title: req.body.title }}, (err, database) => {
    if(err) return res.status(400).json({ message: 'An error occured' })
    res.status(200).json(req.body)
  })
})

app.delete('/delete/:_id', (req, res) => {
  if(req.params._id.length !== 24) res.status(400).json({ 'message': 'Invalid ID' })
  db.db('todos').collection('todos').remove({_id: ObjectId(req.params._id)}, (err, result) => {
    if(err) return res.status(400).json({ message: 'An error occured' })
    res.status(200).send()
  })
})
