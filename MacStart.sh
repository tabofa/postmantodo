#!/bin/bash
echo " ____           _   __  __              "
echo "|  _ \ ___  ___| |_|  \/  | __ _ _ __   "
echo "| |_) / _ \/ __| __| |\/| |/ _\` | '_ \ "
echo "|  __/ (_) \__ \ |_| |  | | (_| | | | | "
echo "|_|   \___/|___/\__|_|  |_|\__,_|_| |_| "
echo  A short tutorial on how to use it...
echo 
echo Starting up backend...
docker-compose -f Backend/docker-compose.yml up -d

echo Starting up fronend...
docker-compose -f Frontend/docker-compose.yml up -d

echo Restarting backend
docker restart backend_backend_1

echo Navigate to http://localhost:3000
